#Import directives
import socket
import time

#Function Code
def printProgramInformation():
    applicationName = ( \
        " ______     ______   ______     __   __     ______     ______     __  __     ______   ______     ______\n" \
        "/\  ___\   /\__  _\ /\  __ \   /\ \"-.\ \   /\  ___\   /\  == \   /\ \_\ \   /\__  _\ /\  ___\   /\  == \\ \n"
        "\ \___  \  \/_/\ \/ \ \ \/\ \  \ \ \-.  \  \ \  __\   \ \  __<   \ \____ \  \/_/\ \/ \ \  __\   \ \  __< \n" \
        " \/\_____\    \ \_\  \ \_____\  \ \_\\\"\_ \  \ \_____\  \ \_____\  \/\_____\    \ \_\  \ \_____\  \ \_\ \_\\ \n"
        "  \/_____/     \/_/   \/_____/   \/_/ \/_/   \/_____/   \/_____/   \/_____/     \/_/   \/_____/   \/_/ /_/ \n")
    print applicationName
    print "V1.0"
    print "Developed by: 01110100 01100001 01101100 01101001 01110011"
    print "Make a selection from the menu below to continue..."
    print ""

def printAvailableCommands():
    print "Available Commands:"
    print "Request Statistics"
    print "Current Working Directory"
    print "List Directory"
    print "Travel Up"
    print "Change Directory"
    print "Display File"
    print "Close Current"
    print "Quit\n"

def printErrorMessage(errorTypeFlag):
    if errorTypeFlag == "INVALID_COMMAND":
        print "Unable to continue."
        print "The command you provided does not exist."
        print "Please double check your input and try again."
    elif errorTypeFlag == "INVALID_IP_ADDRESS":
        print "Unable to continue."
        print "The IP address you provided was invalid."
        print "Please double check your input and try again."
    elif errorTypeFlag == "NO_DATA_PRESENT":
        print "Unable to continue."
        print "No data present in the most previous data transmission.  Please try again later"
        print "StoneByter closed the current connection attempt and exited."
    elif errorTypeFlag == "NO_FILE_SELECTED":
        print "Unable to continue."
        print "You didn't select a file to display."
        print "Please enter a file name in order to execute this command."
    elif errorTypeFlag == "NO_DIRECTORY_SELECTED":
        print "Unable to continue."
        print "You didn't select a directory to change to."
        print "Please enter a directory name in order to execute this command."

# Application Code
def applicationMain():
    # Variable declarations
    serverAddress = ""
    serverPort = 6666
    currentCommand = ""
    socketObject = None
    serverResponse = ""
    serverResponseDecoded = ""
    fileName = ""

    printProgramInformation()

    # Get input from user - serverAddress
    serverAddress = raw_input("Server Address>")
    if serverAddress == "":
        while serverAddress == "":
            printErrorMessage("INVALID_IP_ADDRESS")

            # Get input from user - serverAddress
            serverAddress = raw_input("Server Address>")

    #Generate socket connection
    print "\nGenerating connection to server...\n"
    socketObject = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socketObject.connect((serverAddress, serverPort))

    #Enter command loop
    print "Entering command loop...\n"
    while currentCommand != "quit":
        printAvailableCommands()

        #Get input from user - currentCommand
        currentCommand = raw_input("Command>")
        if currentCommand != "request statistics" and currentCommand != "current working directory" \
                and currentCommand != "list directory" and currentCommand != "travel up" \
                and currentCommand != "close current" and currentCommand != "display file" \
                and currentCommand != "change directory" and currentCommand != "quit":
            while currentCommand != "request statistics" and \
                            currentCommand != "current working directory" \
                    and currentCommand != "list directory" and currentCommand != "travel up" \
                    and currentCommand != "close current" and currentCommand != "display file" \
                    and currentCommand != "change directory" and currentCommand != "quit":
                # Print an error message if the command was invalid
                printErrorMessage("INVALID_COMMAND")

                # Get input from user - currentCommand
                currentCommand = raw_input("Command>")

        #Determine which command to execute
        if currentCommand == "request statistics":
            #Send the command to the server and get the server's response
            socketObject.sendall(currentCommand.encode())
            while True:
                returnedServerResponse = socketObject.recv(1024)
                print "\nTarget Statistics>"
                print returnedServerResponse.decode("UTF-8")
                print ""
        elif currentCommand == "current working directory":
            socketObject.sendall(currentCommand.encode())
            returnedServerResponse = socketObject.recv(1024)
            print "\nCurrent Working Directory>"
            print returnedServerResponse.decode("UTF-8")
            print ""
        elif currentCommand == "list directory":
            socketObject.sendall(currentCommand.encode())
            returnedServerResponse = socketObject.recv(1024)
            print "\nDirectory Listing>"
            print returnedServerResponse.decode("UTF-8")
            print ""
        elif currentCommand == "travel up":
            socketObject.sendall(currentCommand.encode())
            returnedServerResponse = socketObject.recv()
            print returnedServerResponse.decode("UTF-8")
            print ""
        elif currentCommand == "display file":
            #Get input from user - fileName
            fileName = raw_input("File Name>")
            if fileName == "":
                while fileName == "":
                    printErrorMessage("NO_FILE_SELECTED")

                    #Get input from user - fileName
                    fileName = raw_input("File Name>")
            socketObject.sendall(currentCommand.encode())
            socketObject.sendall(fileName.encode())
            returnedServerResponse = socketObject.recv(1024)
            print "\nFile Listing>"
            print returnedServerResponse.decode("UTF-8")
            print ""
        elif currentCommand == "change directory":
            #Get input form user - directoryName
            directoryName = raw_input("Directory Name>")
            if directoryName == "":
                while directoryName == "":
                    printErrorMessage("NO_DIRECTORY_SELECTED")

                    #Get input form user - directoryName
                    directoryName = raw_input("Directory Name>")
            socketObject.sendall(currentCommand.encode())
            socketObject.sendall(directoryName.encode())
            returnedServerResponse = socketObject.recv(1024)
            print "directory output>",returnedServerResponse.decode("UTF-8")
            # print ""
        elif currentCommand == "close current":
            socketObject.sendall(currentCommand.encode())
            returnedServerResponse = socketObject.recv(1024)
            print returnedServerResponse.decode("UTF-8")
        elif currentCommand == "quit":
            print "Exiting..."


applicationMain()
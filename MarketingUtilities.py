#Import directives
import socket
import platform
import subprocess
import os
import time

#Function Code
def getServerStatistics():
    #Variable declarations
    machineType = platform.machine()
    networkName = platform.node()
    platformInformation = platform.platform()
    processorInformation = platform.processor()

    #Format output before appending to returnList
    machineTypeFormatted = "Machine Type=" + machineType
    networkNameFormatted = "Network Name=" + networkName
    platformInformationFormatted = "Platform Information=" + platformInformation
    processorInformationFormatted = "Processor Information=" + processorInformation

    returnList = [machineTypeFormatted, networkNameFormatted, platformInformationFormatted,
                  processorInformationFormatted]
    return returnList

def directoryHandler(commandFlag, directoryName=""):
    #Determine which command flag was chosen
    if commandFlag == "TRAVEL_UP" and directoryName != "":
        #If the current command is TRAVERSE_UP then eexecute the following equivalent shell command: cd ..
        try:
            os.chdir(directoryName)
            travelUpStatusMessage = "Traveled up one directory."
        except OSError:
            travelUpStatusMessage = "Unable to travel up the directory hierarchy."
        return travelUpStatusMessage
    elif commandFlag == "CHANGE_DIRECTORY" and directoryName != "":
        #If the current command is CHANGE_DIRECTORY then execute the following equivalent
        #shell command: cd <directory name>
        try:
            os.chdir(directoryName)
            changeDirectoryStatusMessage = "Changed directories."
        except OSError:
            changeDirectoryStatusMessage = "Unable to change directories."
        return changeDirectoryStatusMessage
    elif commandFlag == "CURRENT_WORKING_DIRECTORY":
        #If the current command is CURRENT_WORKING_DIRECTORY then execute the
        #following equivalent shell command: pwd
        currentShellCommand = "pwd"
        currentWorkingDirectoryOutputAsBytes = subprocess.check_output(currentShellCommand)
        currentWorkingDirectoryOutputAsString = currentWorkingDirectoryOutputAsBytes.decode("UTF-8")
        return currentWorkingDirectoryOutputAsString
    elif commandFlag == "DIRECTORY_LISTING":
        #If the current command is DIRECTORY_LISTING, then execute the following equivalent
        #shell command - ls -l
        currentShellCommand = "ls"
        currentShellCommandSwitch = "-l"

        #Get the directory listing output and return back from the function
        directoryListingOutputAsString = subprocess.check_output([currentShellCommand, currentShellCommandSwitch])
        return directoryListingOutputAsString

def fileHandler(commandFlag, fileName):
    if commandFlag == "DISPLAY_FILE" and fileName != "":
        #If the current command is DISPLAY_FILE then execute the following equivalent
        #shell command: cat <file name>
        currentShellCommand = "cat"
        try:
            fileCatOutputAsBytes = subprocess.check_output([currentShellCommand, fileName])
            fileCatOutputAsString = fileCatOutputAsBytes.decode("UTF-8")
        except OSError:
            errorMessage = "Unable to open that file."
            return errorMessage
        return fileCatOutputAsString

#Application Code
def applicationMain():
    #Variable declarations
    socketObject = None
    serverAddress = ""
    serverPort = 0
    compiledServerStatistics = ""
    incomingConnectionObject = None

    #Set server address and expected port number
    serverAddress = socket.gethostbyname(socket.gethostname())
    serverPort = 6666

    #Create a socket connection
    socketObject = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socketObject.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    socketObject.bind((serverAddress, serverPort))
    socketObject.listen(1)

    #Begin listening for incoming client connections
    incomingConnectionObject, incomingClientAddress = socketObject.accept()
    while True:
        #On incoming connection - get expected data
        try:
            currentCommand = incomingConnectionObject.recv(1024)
        except:
            continue

        #Decode the incoming data and determine which client command
        #was passed to the server
        #Expected Commands:
        #request statistics
        #current working directory
        #list directory
        #travel up
        #change directory
        #display file
        #close current
        currentCommandDecoded = currentCommand.decode("UTF-8")
        if currentCommandDecoded == "request statistics":
            #If the current command is request server statistics then get server
            #statistics and encode for sending back
            serverStatistics = getServerStatistics()
            for currentStat in serverStatistics:
                compiledServerStatistics += currentStat + "\n"

            #Send the compiled server statistics back to the client
            incomingConnectionObject.sendall(compiledServerStatistics.encode())
        elif currentCommandDecoded == "current working directory":
            #If the current command is [current working directory] then execute the associated
            #command on the shell
            currentWorkingDirectory = directoryHandler("CURRENT_WORKING_DIRECTORY")

            #Send the current working directory to the client
            incomingConnectionObject.sendall(currentWorkingDirectory.encode())
        elif currentCommandDecoded == "list directory":
            #If the current command is [list directory] then execute the associated
            #command on the shell
            directoryListing = directoryHandler("DIRECTORY_LISTING")

            #Send the directory listing to the client
            incomingConnectionObject.sendall(directoryListing.encode())
        elif currentCommandDecoded == "travel up":
            #If the current command is travel up then execute the TRAVERSE_UP command flag option
            traverseUpShellOutput = directoryHandler("TRAVEL_UP","..")
            incomingConnectionObject.sendall(traverseUpShellOutput.encode())
        elif currentCommandDecoded == "change directory":
            #Get the directory name from the client
            # time.sleep(1)
            directoryNameAsBytes = incomingConnectionObject.recv(1024)
            directoryNameAsString = directoryNameAsBytes.decode("UTF-8")

            changeDirectoryShellOutput = directoryHandler("CHANGE_DIRECTORY", directoryNameAsString)
            incomingConnectionObject.sendall(changeDirectoryShellOutput.encode())
        elif currentCommandDecoded == "display file":
            #Get the file name from the client
            fileNameAsBytes = incomingConnectionObject.recv(1024)
            fileNameAsString = fileNameAsBytes.decode("UTF-8")

            #If the current command is [display file] then execute the associated command on the shell
            #and send back to the client
            fileDisplayCommandOutput = fileHandler("DISPLAY_FILE", fileNameAsString)
            incomingConnectionObject.sendall(fileDisplayCommandOutput.encode())
        elif currentCommandDecoded == "close current":
            #If the current command is to close the current connection then close and
            #reset for further incoming connections
            closeConnectionMessage = "Closing this session."
            incomingConnectionObject.sendall(closeConnectionMessage.encode())
            incomingConnectionObject.shutdown(0)

            #Reinitialize for incoming connections
            incomingConnectionObject, incomingClientAddress = socketObject.accept()

applicationMain()